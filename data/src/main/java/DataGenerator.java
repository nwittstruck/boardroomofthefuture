import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class DataGenerator {

	public static void main(String[] args) throws SQLException, ParseException {
        Connection conn = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/me310",
				"nicholas", "");
		Statement stmt = conn.createStatement();
		
		//Expenses Table
		try {
			stmt.executeUpdate("DROP TABLE DEP_EXPENSES");
		} catch (Exception e) {}
		stmt.executeUpdate("CREATE TABLE DEP_EXPENSES ("
				+ "expense_key INTEGER NOT NULL,"
				+ "dep_key INTEGER NOT NULL,"
				+ "date DATE,"
				+ "category VARCHAR(15),"
				+ "subcategory VARCHAR(10),"
				+ "amount INTEGER);");
		
		Random r = new Random();
		long diff = new Date().getTime() - new SimpleDateFormat("yyyy-MM-dd").parse("2012-01-01").getTime();
		long diff2 = new Date().getTime() - new SimpleDateFormat("yyyy-MM-dd").parse("2013-01-01").getTime();
		
		String[] categories = {"MATERIALS", "INFRASTRUCTURE", "EMPLOYEE"};
		String[] subcategories = {"PAY", "SOCIAL"};
		String[] costtypes = {"TRAIN", "FLIGHT", "HOTEL", "T&E"};
		String[] countries = {"INDIA", "USA", "CHINA", "GERMANY", "DENMARK", "NETHERLANDS", "FRANCE", "UK", "MEXICO"};
		String[] sectors = {"Wind Parks", "Healthcare", "Appliances", "Trains", "Other"};

		//Various Expenses
		int i=0;
		for (; i<100; i++) {
			Date date = new Date((long) (new Date().getTime() - r.nextInt(1000) * diff/1000));
			stmt.executeUpdate("INSERT INTO DEP_EXPENSES "
					+ "values("+ i +", " 
					+ r.nextInt(5) +", '"
					+ date + "', '"
					+ categories[r.nextInt(categories.length)] +"', '"
					+ subcategories[r.nextInt(subcategories.length)] +"', "
					+ r.nextInt(10000) + " );");
		}
		
		//Travel Table
		try {
			stmt.executeUpdate("DROP TABLE TRAVEL_EXPENSES");
		} catch (Exception e) {}		
		stmt.executeUpdate("CREATE TABLE TRAVEL_EXPENSES ("
				+ "expense_key INTEGER NOT NULL,"
				+ "employee_id INTEGER,"
				+ "destination VARCHAR(255),"
				+ "costtype VARCHAR(10));");
		
		
		//Travel Expenses
		for (; i<120; i++) {
			Date date = new Date((long) (new Date().getTime() - r.nextInt(1000) * diff/1000));
			stmt.executeUpdate("INSERT INTO TRAVEL_EXPENSES "
					+ "values("+ i +", " 
					+ r.nextInt() +", '"
					+ countries[r.nextInt(countries.length)] + "', '"
					+ costtypes[r.nextInt(costtypes.length)] +"');");
			
			stmt.executeUpdate("INSERT INTO DEP_EXPENSES "
					+ "values("+ i +", " 
					+ r.nextInt(5) +", '"
					+ date + "', "
					+ "'EMPLOYEE', "
					+ "'TRAVEL', "
					+ r.nextInt(10000) + " );");
		}
		
		//And some more Travels to India by Dep. 4 in 2013
		for (; i<150; i++) {
			stmt.executeUpdate("INSERT INTO TRAVEL_EXPENSES "
					+ "values("+ i +", " 
					+ r.nextInt() +", "
					+ "'INDIA', '"
					+ costtypes[r.nextInt(costtypes.length)] +"');");
			
			Date date = new Date((long) (new Date().getTime() - r.nextInt(1000)*diff2/1000 ));
			stmt.executeUpdate("INSERT INTO DEP_EXPENSES "
					+ "values("+ i +", " 
					+ "4 , '"
					+ date + "', "
					+ "'EMPLOYEE', "
					+ "'TRAVEL', "
					+ r.nextInt(10000) + " );");
		}
		
		//Revenue Table
		try {
			stmt.executeUpdate("DROP TABLE REVENUES");
		} catch (Exception e) {}
		stmt.executeUpdate("CREATE TABLE REVENUES ("
				+ "revenue_key INTEGER NOT NULL,"
				+ "sector VARCHAR(20),"
				+ "date DATE,"
				+ "project VARCHAR(255),"
				+ "country VARCHAR(20),"
				+ "amount INTEGER);");
		
		//Various Revenues
		i=0;
		for (; i<100; i++) {
			Date date = new Date((new Date().getTime() - r.nextInt(1000) * diff/1000));
			stmt.executeUpdate("INSERT INTO REVENUES "
					+ "values("+ i +", '" 
					+ sectors[r.nextInt(sectors.length)] + "', '"
					+ date + "', "
					+ "NULL, '"
					+ countries[r.nextInt(6)] + "', "
					+ r.nextInt(10000) + " );");
		}
		
		//More Wind Park Revenues in the Netherlands in 2013
		for (; i<120; i++) {
			Date date = new Date((new Date().getTime() - r.nextInt(1000) * diff2/1000));
			stmt.executeUpdate("INSERT INTO REVENUES "
					+ "values("+ i +", " 
					+ "'Wind Parks', '"
					+ date + "', "
					+ "NULL, "
					+ "'NETHERLANDS', "
					+ r.nextInt(10000) + " );");
		}
		
		
		
	}

}
