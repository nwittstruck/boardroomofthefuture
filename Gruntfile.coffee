"use strict"

module.exports = (grunt) -> 
  require('load-grunt-tasks')(grunt)

  # Project configuration.

  config = 
    buildcontrol:
      options:
        dir: "dist"
        commit: true
        push: true
        message: "Built %sourceName% from commit %sourceCommit% on branch %sourceBranch%"
        connectCommits: false

      heroku:
        options:
          remote: "git@heroku.com:botf.git"
          branch: "master"

      local:
        options:
          remote: "../"
          branch: "build"
    sync:
      main:
        files: [
            expand: true
            cwd: 'backend/'
            src: ['**']
            dest: 'dist/'
            rename: (cwd, filename) ->
              if filename is 'gitignore'
                return cwd + '.' + filename 
              return cwd + filename
        ]
    clean: [
      '.tmp',
      'dist/*',
      '!dist/.git*'
    ]

  grunt.initConfig config
  
  grunt.registerTask 'deploy', ['build', 'buildcontrol:heroku']

  grunt.registerTask 'build', 'desc', ->
  #  grunt.file.setBase 'frontend'
   # grunt.task.run ['build']
    #grunt.file.setBase '..'
    grunt.task.run 'clean'
    grunt.task.run 'sync'