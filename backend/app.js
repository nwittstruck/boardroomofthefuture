var io = require('socket.io').listen(+process.env.PORT || 3000);

io.set('transports',[
  'websocket',
  'xhr-polling'
]);

var lastQuery;
var lastSlide;

io.sockets.on('connection', function (socket) {
  if (lastQuery) {
    //socket.emit('query', lastQuery);
  }
  //if (lastSlide) {
  //  socket.emit('slide', lastSlide);
  //}

  socket.on('query', function (data) {
    lastQuery = data;
    socket.broadcast.emit('query', data);     
  });

  socket.on('touch', function (data) {
    data.socketID = socket.id;
    //socket.emit('touch', data);
    socket.broadcast.emit('touch', data);
  });

  socket.on('slide', function (data) {
    console.log("received slide");
    lastSlide = data;
    socket.broadcast.emit('slide', data);     
  });

  socket.on('debug', function (data) {
    console.log('DEBUG: ' + data);
  })
});