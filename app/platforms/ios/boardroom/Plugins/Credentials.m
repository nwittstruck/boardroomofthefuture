//
//  Credentials.m
//  PhoneGapSpeechTest
//
//  Created by Adam on 10/15/12.
//
//

#import "Credentials.h"


const unsigned char SpeechKitApplicationKey[] = {0x0d, 0x91, 0x89, 0x4b, 0xab, 0x99, 0xbe, 0xd3, 0xa0, 0xca, 0xb2, 0x31, 0x62, 0x34, 0xea, 0x7f, 0xab, 0xb4, 0x54, 0xe9, 0x75, 0x44, 0xd8, 0x4c, 0xf4, 0xa8, 0x90, 0xb8, 0x6d, 0x75, 0xdd, 0x7b, 0x06, 0x66, 0xc6, 0xb1, 0x00, 0x84, 0x84, 0xa0, 0x4e, 0x36, 0xd1, 0xb0, 0x6a, 0xbb, 0x30, 0x16, 0x6f, 0x84, 0xff, 0xe4, 0xc8, 0xd7, 0x31, 0xbc, 0x80, 0xc1, 0x75, 0xed, 0x36, 0x52, 0x68, 0x3c
};

@implementation Credentials 
@synthesize appId, appKey;

NSString* APP_ID = @"NMDPTRIAL_boardroom20140530010743";

-(NSString *) getAppId {
    return [NSString stringWithString:APP_ID];
};

@end
