//
//  MeetingRoomViewController.m
//  boardroom
//
//  Created by Nicholas Wittstruck on 29/05/14.
//
//

#import "MeetingRoomViewController.h"

@interface MeetingRoomViewController ()

@end

@implementation MeetingRoomViewController

@synthesize circles;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.circles setHidden:YES];
    [self.compass setHidden:YES];
    
}

- (void) drawGrowingCircles {
    
    CGPoint touchPoint = CGPointMake(100, 100);
    CALayer *waveLayer=[CALayer layer];
    waveLayer.frame = CGRectMake(touchPoint.x-1, touchPoint.y-1, 10, 10);
    waveLayer.borderColor =[UIColor colorWithRed:0 green:0.65 blue:0.65 alpha:1.0].CGColor;
    waveLayer.borderWidth =0.2;
    waveLayer.cornerRadius =5.0;
    
    [self.circles.layer addSublayer:waveLayer];
    [self scaleBegin:waveLayer];
}

- (void) removeLayer: (NSTimer*) timer {
    CALayer* layer = (CALayer*) [timer userInfo];
    [layer removeFromSuperlayer];
}


- (void) fadeOutLayer: (CALayer*) layer {
    if ([layer.animationKeys count] == 1) {
        int duration = 1;
        CABasicAnimation* theAnimation=[CABasicAnimation animationWithKeyPath:@"opacity"];
        theAnimation.duration=duration;
        theAnimation.fromValue=[NSNumber numberWithFloat:1.0];
        theAnimation.toValue=[NSNumber numberWithFloat:0.0];
        [layer addAnimation:theAnimation forKey:@"animateOpacity"];
        
        [NSTimer scheduledTimerWithTimeInterval:duration
                                         target:self
                                       selector:@selector(removeLayer:)
                                       userInfo:layer
                                        repeats:YES];
    }
}


-(void)scaleBegin:(CALayer *)aLayer
{
    const float maxScale=10.0;
    if (aLayer.transform.m11<maxScale) {
        if (aLayer.transform.m11==1.0) {
            [aLayer setTransform:CATransform3DMakeScale( 1.1, 1.1, 1.0)];
            
        }
        else {
            [aLayer setTransform:CATransform3DScale(aLayer.transform, 1.1, 1.1, 1.0)];
        }
        [self performSelector:_cmd withObject:aLayer afterDelay:0.05];
    }
    else {
        //[aLayer removeFromSuperlayer];
        [self fadeOutLayer:aLayer];
    }
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Get user preference
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL offlineMode = [defaults boolForKey:@"offlineMode"];
    if (offlineMode) {
        [self meetingIsNearby:SOutMeeting];
        [self drawRadar];
        //[[self logo] setHidden: YES];
    }
}

- (void) drawRadar {
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"radar_hayk" withExtension:@"gif"];
    
    UIImage* image = [UIImage animatedImageWithAnimatedGIFURL:url];
    [self.logo setBackgroundColor:[UIColor clearColor]];
    [self.logo setImage:image];
}

- (void) drawCompass {
    
    NSString *zRotationKeyPath = @"transform.rotation.z"; // The killer of typos
    
    // Change the model to the new "end value" (key path can work like this but properties don't)
    CGFloat currentAngle = [[self.compassInner.layer valueForKeyPath:zRotationKeyPath] floatValue];
    CGFloat angleToAdd   = M_PI_2; // 90 deg = pi/2
    [self.compassInner.layer setValue:@(currentAngle + M_PI_2/2) forKeyPath:zRotationKeyPath];
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:zRotationKeyPath];
    animation.duration = 1;
    // @( ) is fancy NSNumber literal syntax ...
    animation.toValue = @(M_PI_2 / 2);
    animation.byValue = @(angleToAdd);
    animation.removedOnCompletion = NO;
    animation.autoreverses = YES;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.repeatCount = NSIntegerMax;
    [self.compassInner.layer addAnimation:animation forKey:@"90rotation"];

}

- (void) viewDidAppear:(BOOL)animated {
    /*[NSTimer scheduledTimerWithTimeInterval:1
                                     target:self
                                   selector:@selector(drawGrowingCircles)
                                   userInfo:nil
                                    repeats:YES];
    */
    //[self drawCompass];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) meetingIsNearby: (SMeetingState) meetingState {
    if (meetingState == SInMeeting) {
        [self.titleLabel setText: @"There is a meeting nearby. Press join to enter the meeting"];
        [self.joinButton setTitle:@"Join" forState:UIControlStateNormal];
    }
    else {
        [self.titleLabel setText: @"There is no meeting nearby, but you can explore your company's data"];
                [self.joinButton setTitle:@"Explore" forState:UIControlStateNormal];
    }
}

- (IBAction)join:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
