//
//  SMeetingState.h
//  boardroom
//
//  Created by Nicholas Wittstruck on 29/05/14.
//
//

#ifndef boardroom_SMeetingState_h
#define boardroom_SMeetingState_h


typedef NS_ENUM(NSInteger, SMeetingState) {
    SInMeeting,
    SOutMeeting
};


#endif
