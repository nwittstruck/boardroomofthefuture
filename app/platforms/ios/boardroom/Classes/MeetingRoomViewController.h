//
//  MeetingRoomViewController.h
//  boardroom
//
//  Created by Nicholas Wittstruck on 29/05/14.
//
//

#import "SMeetingState.h"
#import "UIImage+animatedGIF.h"

@interface MeetingRoomViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *joinButton;
@property (weak, nonatomic) IBOutlet UIView *circles;
@property (weak, nonatomic) IBOutlet UIView *compass;
@property (weak, nonatomic) IBOutlet UIImageView *logo;
@property (weak, nonatomic) IBOutlet UIImageView *compassInner;
- (IBAction)join:(id)sender;
- (void) meetingIsNearby: (SMeetingState) meetingState;

@end
