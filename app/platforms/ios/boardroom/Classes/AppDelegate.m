/*
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 */

//
//  AppDelegate.m
//  boardroom
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright ___ORGANIZATIONNAME___ ___YEAR___. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"

#import <Cordova/CDVPlugin.h>

@implementation AppDelegate

@synthesize window, viewController;

- (id)init
{
    /** If you need to do any extra app-specific initialization, you can do it here
     *  -jm
     **/
    NSHTTPCookieStorage* cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];

    [cookieStorage setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];

    int cacheSizeMemory = 8 * 1024 * 1024; // 8MB
    int cacheSizeDisk = 32 * 1024 * 1024; // 32MB
#if __has_feature(objc_arc)
        NSURLCache* sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:cacheSizeMemory diskCapacity:cacheSizeDisk diskPath:@"nsurlcache"];
#else
        NSURLCache* sharedCache = [[[NSURLCache alloc] initWithMemoryCapacity:cacheSizeMemory diskCapacity:cacheSizeDisk diskPath:@"nsurlcache"] autorelease];
#endif
    [NSURLCache setSharedURLCache:sharedCache];

    self = [super init];
    return self;
}

#pragma mark UIApplicationDelegate implementation

/**
 * This is main kick off after the app inits, the views and Settings are setup here. (preferred - iOS4 and up)
 */
- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
    [self setDefaultSettings];
    // [self initializeBeacons];
    
    return YES;
}

- (void) initializeBeacons {
    // ibeacon support:
    
    // This location manager will be used to notify the user of region state transitions when the app has been previously terminated.
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.meetingState = SOutMeeting; // SOutMeeting;
    
    [self.locationManager startMonitoringForRegion:[self beaconRegion]];
    //[self.locationManager startRangingBeaconsInRegion:[self beaconRegion]];
    
    /*[NSTimer scheduledTimerWithTimeInterval:10
     target:self
     selector:@selector(push)
     userInfo:nil
     repeats:YES];*/
    
}

- (void) setDefaultSettings {
    // Set the application defaults
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *appDefaults = [NSDictionary dictionaryWithObject:@"http://172.16.53.91:3000"
                                                            forKey:@"serverUrl"];
    [defaults registerDefaults:appDefaults];
    [defaults synchronize];

}

- (void) push {
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.alertBody = [NSString stringWithFormat:@"You are in the Q: Meeting Room 1"];
    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
}

- (CLBeaconRegion*) beaconRegion {
    NSUUID* _myUUID = [[NSUUID alloc] initWithUUIDString:@"F7826DA6-4FA2-4E98-8024-BC5B71E0893E"];
    CLBeaconRegion* beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:_myUUID major:47749 minor:15255 identifier:kUniqueRegionIdentifier];
    beaconRegion.notifyEntryStateOnDisplay = YES;
    beaconRegion.notifyOnEntry = YES;
    beaconRegion.notifyOnExit = YES;
    return beaconRegion;
}

// Delegate method from the CLLocationManagerDelegate protocol.
- (void)locationManager:(CLLocationManager *)manager
        didRangeBeacons:(NSArray *)beacons
               inRegion:(CLBeaconRegion *)region {
    
    MainViewController* mainViewController = (MainViewController*)self.window.rootViewController;
    
    if ([beacons count] > 0) {
        CLBeacon *nearestExhibit = [beacons firstObject];
        UILocalNotification *notification = [[UILocalNotification alloc] init];
        // Present the exhibit-specific UI only when
        // the user is relatively close to the exhibit.
        CLProximity proximity = nearestExhibit.proximity;
        if ((CLProximityNear == nearestExhibit.proximity || CLProximityImmediate == nearestExhibit.proximity) && self.meetingState == SOutMeeting) {
            notification.alertBody = [NSString stringWithFormat:@"You are in the Q: %@", region.identifier];
            //[[UIApplication sharedApplication] presentLocalNotificationNow:notification];
            self.meetingState = SInMeeting;
            
        } else if ((CLProximityFar == nearestExhibit.proximity || CLProximityUnknown == nearestExhibit.proximity) && self.meetingState == SInMeeting) {
            notification.alertBody = [NSString stringWithFormat:@"You are in the Q: %@", region.identifier]; //@"You've left the meeting: %@"
            //[[UIApplication sharedApplication] presentLocalNotificationNow:notification];
            self.meetingState = SOutMeeting;
        }
        
        [mainViewController updateMeetingRoomSelection: self.meetingState];
        //[mainViewController updateMeetingRoomSelection: SInMeeting];
    }
}

- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region {
    [self.locationManager startRangingBeaconsInRegion:[self beaconRegion]];
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    [self.locationManager startRangingBeaconsInRegion:[self beaconRegion]];

    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.alertBody = [NSString stringWithFormat:@"You are in the Q: %@", region.identifier];
    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
}

-(void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    [self.locationManager stopRangingBeaconsInRegion:[self beaconRegion]];
}

// this happens while we are running ( in the background, or from within our own app )
// only valid if boardroom-Info.plist specifies a protocol to handle
- (BOOL)application:(UIApplication*)application handleOpenURL:(NSURL*)url
{
    if (!url) {
        return NO;
    }

    // calls into javascript global function 'handleOpenURL'
    NSString* jsString = [NSString stringWithFormat:@"handleOpenURL(\"%@\");", url];
    [self.viewController.webView stringByEvaluatingJavaScriptFromString:jsString];

    // all plugins will get the notification, and their handlers will be called
    [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:CDVPluginHandleOpenURLNotification object:url]];

    return YES;
}

// repost the localnotification using the default NSNotificationCenter so multiple plugins may respond
- (void)            application:(UIApplication*)application
    didReceiveLocalNotification:(UILocalNotification*)notification
{
    // re-post ( broadcast )
    [[NSNotificationCenter defaultCenter] postNotificationName:CDVLocalNotification object:notification];
}

- (NSUInteger)application:(UIApplication*)application supportedInterfaceOrientationsForWindow:(UIWindow*)window
{
    // iPhone doesn't support upside down by default, while the iPad does.  Override to allow all orientations always, and let the root view controller decide what's allowed (the supported orientations mask gets intersected).
    NSUInteger supportedInterfaceOrientations = (1 << UIInterfaceOrientationPortrait) | (1 << UIInterfaceOrientationLandscapeLeft) | (1 << UIInterfaceOrientationLandscapeRight) | (1 << UIInterfaceOrientationPortraitUpsideDown);

    return supportedInterfaceOrientations;
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication*)application
{
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

@end
