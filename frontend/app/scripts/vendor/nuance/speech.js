
var speechKit = new NuanceSpeechKitPlugin();

function setRecoDialogVisibile(visibility) {
    recoDialog = document.getElementById("reco-dialog");
    if (visibility == true){
        recoDialog.style.visibility = "visible";
    }
    else{
        recoDialog.style.visibility = "hidden";
    }
}

function toggleRecoDialog() {
    recoDialog = document.getElementById("reco-dialog");
    recoDialog.style.visibility = (recoDialog.style.visibility == "visible") ? "hidden" : "visible";
}


function doInit() {
    var serverURL = "sandbox.nmdp.nuancemobility.net";
    speechKit.initialize("Credentials", serverURL, 443, false, function(r){printResult(r)}, function(e){printResult(e)} );
}

function doCleanup(){
    speechKit.cleanup( function(r){printResult(r)}, function(e){printResult(e)} );
}

function startRecognition(){
    printRecoStage("Listening");
    var recoType = "dictation";
    var recoLanguage = "en_us"
    var stopButton = document.getElementById("stop-reco");
    stopButton.disabled = false;
    
    speechKit.startRecognition(recoType, recoLanguage, function(r){printRecoResult(r)}, function(e){printRecoResult(e)} );
    setRecoDialogVisibile(true);
    var tempObj = new Object();
    document.getElementById('debug').innerHTML="Loading...";
}

function stopRecognition(){
    var stopButton = document.getElementById("stop-reco");
    stopButton.disabled = true;
    speechKit.stopRecognition(function(r){printRecoResult(r)}, function(e){console.log(e)} );
    
}

function getResult(){
    speechKit.getResults(function(r){printResult(r)}, function(e){console.log(e)} );
}

function printRecoResult(resultObject){
    if (resultObject.event == 'RecoVolumeUpdate'){
        setVolumeLevel(resultObject);
    }
    else{
        if (resultObject.event == 'RecoComplete' || resultObject.event == 'RecoStopped' || resultObject.event == 'RecoError'){
            var stopButton = document.getElementById("stop-reco");
            stopButton.disabled = true;
            setRecoDialogVisibile(false);
        }
        
        if (resultObject.event == 'RecoComplete' || resultObject.event == 'RecoError') {
            var innerHtmlText=getHtml(resultObject);
            document.getElementById("result").innerHTML=innerHtmlText;
            
            var iframe = document.getElementById("webapp");
            
            if (resultObject.results != undefined){
                var element = iframe.contentWindow.document.getElementById('nuanceSpeechInput');
                element.value=resultObject.results[0].value;
                document.getElementById('debug').innerHTML=resultObject.results[0].value;
                
                iframe.contentWindow.updatedata(resultObject.results[0].value);
                if ("createEvent" in document) {
                    var evt = document.createEvent("HTMLEvents");
                    evt.initEvent("change", false, true);
                    element.dispatchEvent(evt);
                }
                else
                    element.fireEvent("onchange");
            }
            else {
                document.getElementById('debug').innerHTML="No result";
            }
            
            playTTS();
        }
    }
}

function playTTS(){
    var text = document.getElementById('debug').innerHTML;
    
    if (text.length > 0) {
        if (text == "No result") {
            text = "Sorry, I couldn't understand you.";
            say(text);
        }
        else {
            text = "Here is what I understood: " + text;
        }

        //say(text);
    }
}

function say(text) {
    console.log(text);

    var ttsLanguage = "en_us"
    speechKit.playTTS(text, ttsLanguage, "Serena", function(r){}, function(e){} );
}

function printResult(resultObject){
    var innerHtmlText=getHtml(resultObject);
    document.getElementById("result").innerHTML=innerHtmlText;
}

function printRecoStage(stage){
    var resultObject = new Object();
    resultObject.event = stage;
    var innerHtmlText=getHtml(resultObject);
    document.getElementById("result").innerHTML=innerHtmlText;
}

function setVolumeLevel(resultObject){
    var htmlText=" Volume: "+resultObject.volumeLevel;
    document.getElementById("volume-level").innerHTML=htmlText;
}

function getHtml(resultObject){
    var htmlText="<ul><li>Return Code: "+resultObject.returnCode;
    htmlText=htmlText+"</li></ul>";
    htmlText=htmlText+"<ul><li>Return Text: "+resultObject.returnText;
    htmlText=htmlText+"</li></ul>";
    if (resultObject.result != undefined){
        htmlText=htmlText+"<ul><li>Result: "+resultObject.result;
        htmlText=htmlText+"</li></ul>";
    }
    if (resultObject.results != undefined){
        var resultCount = resultObject.results.length;
        var i = 0;
        htmlText=htmlText+"<ul><li>Results Details:";
        for (i = 0; i < resultCount; i++){
            htmlText=htmlText+"<br>"+i+": "+resultObject.results[i].value+" ["+resultObject.results[i].confidence+"]";
        }
        htmlText=htmlText+"</li></ul>";
    }
    if (resultObject.event != undefined){
        htmlText=htmlText+"<ul><li>Event: "+resultObject.event;
        htmlText=htmlText+"</li></ul>";
    }
    return htmlText;
}
