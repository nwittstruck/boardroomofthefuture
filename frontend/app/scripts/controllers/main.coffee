groupByCriteriaMapping = 
  'country': 'barchart'
  'lead_country': 'countrymap'
  'period': 'linechart'
  'sector': 'barchart'
  'division': 'barchart'
  'subdivision': 'barchart'
  'business unit': 'barchart'
  'business segment': 'barchart'
  'business subsegment': 'barchart'
  'undefined' : 'number'


angular.module("botfApp").controller "MainCtrl", ($scope, $location, $compile, $route, $sce, NLProcessingService, speechService, graphService, lineitems) ->
  
  # first, just use the localhost
  syncServerUrl = "http://localhost:3000"

  socket = io.connect syncServerUrl, { 'connnect timeout': 5000 }
  console.log("connected to server")

  window.setServerUrl = (serverUrl) ->
    console.log("disconnecting old socket")
    socket.disconnect();
    delete io.sockets["http://localhost:3000"];
    io.j = [];
    console.log("connecting to: " + serverUrl)
    socket = io.connect(serverUrl, { 'connnect timeout': 5000 });    

  $scope.state = "query"
  $scope.blockSlideChanged = false;

  slideChanged = (event) ->
    console.log("slideChanged")    
    data =
      indexv: Reveal.getIndices().v
      indexh: Reveal.getIndices().h
      indexf: Reveal.getIndices().f or 0
    #if !$scope.blockSlideChanged
    corresponingHistoryItem = _.findWhere($scope.history(),
      slideNr: data.indexh)
    if corresponingHistoryItem?.parameters?
      if $scope.queryParameters != corresponingHistoryItem.parameters
        $scope.blockSlideChanged = true;
        $scope.queryParameters = _.cloneDeep corresponingHistoryItem.parameters
        $scope.sendQueryChanged(corresponingHistoryItem.slideNr)
    #else
    #  $scope.blockSlideChanged = false
    ###
    data =
      indexv: Reveal.getIndices().v
      indexh: Reveal.getIndices().h
      indexf: Reveal.getIndices().f or 0

    if lastReceivedSlide isnt data.indexh
      socket.emit "slide", data
      lastReceivedSlide = `undefined`###


  $scope.startSync = () ->
    Reveal.initialize history: false, touch: false, progress: false, embedded: true, overview: false
    #Reveal.addEventListener "slidechanged", slideChanged
    #Reveal.addEventListener "fragmentshown", slideChanged
    #Reveal.addEventListener "fragmenthidden", slideChanged

  ###
  socket.on "slide", (data) ->
    lastReceivedSlide = data.indexh
    Reveal.slide data.indexh, data.indexv, data.indexf###
    
  $scope.goToSlide = (slideNr) ->
    Reveal.slide slideNr, 0, 0
  


  # Run slides startSync
  $scope.startSync()

  $scope.trustSrc = (src) ->
    $sce.trustAsResourceUrl src
  
  $scope.carouselNext = ->
    $scope.carouselIndex++
  $scope.carouselPrev = ->
    $scope.carouselIndex--
    
  $scope.$on '$locationChangeSuccess', (event, next, current) ->
    $scope.carouselIndex = $route.current?.$$route?.carouselIndex
    $scope.carouselIndex = $scope.carouselIndex or 0

  $scope.$watch 'carouselIndex', ->
    return if not $scope.carouselIndex?
    route = _.find $route.routes, 
      carouselIndex: $scope.carouselIndex
    $location.path(route.originalPath)

    #if $scope.carouselIndex == 0
    #  $scope.ann.on = false
    #else
    #  $scope.ann.on = true
  
  $scope.textEntryFieldChanged = (text)->
    return if not text 
    return if text is ""
    if text[-1..] is " "
      $scope.recognition.currentQuery = text
      
  $scope.submitEntryField = (text)->
    $scope.recognition.currentQuery = text
  
  socket.on "query", (data) -> #undochange
    #if !$scope.sentQuery
    console.log("socket query")
    if data.queryParameters?[0]?
      $scope.queryParameters = data.queryParameters
      updateInputField()
      if data.slideNumber?
        $scope.goToSlide(data.slideNumber)
      $scope.$apply()

    # update the agendaItem
    # compare to agenda
    # console.log($scope.queryParameters)
    updateCurrentAgendaItem()

  ###
  socket.on "slide", (data) ->
    if !$scope.sentSlide    
      corresponingHistoryItem = _.findWhere($scope.agenda,
        slideNr: data.indexh)
      if corresponingHistoryItem?.parameters?
        $scope.queryParameters = _.cloneDeep corresponingHistoryItem.parameters
        $scope.$apply()
    else
      $scope.sentSlide = false###

  # --------- Annotation -----------------------------

  # Object holding annotation data
  $scope.ann = Object()

  # Variable deciding whether annotation is on
  $scope.ann.on = true

  # Object holding history of states for all clients
  $scope.ann.data = {}

  # How many milliseconds trails are stored for
  $scope.ann.trailLife = 1500

  # How long to wait for no events before we start erasing
  $scope.ann.waitDuration = 1500

  # Is this device currently pressing?
  $scope.ann.pressed = false

  # UUID associated with the current press
  $scope.ann.press_id

  # HTML canvas to draw on
  $scope.ann.canvas = $('#annotation-canvas')[0]

  setTimeout (->

    if $scope.tablet
      $scope.ann.canvas_width = 1024
      $scope.ann.canvas_height = 768
    else 
      $scope.ann.canvas_width = $('.botf-wrapper').width()
      $scope.ann.canvas_height = $('.botf-wrapper').height()

    #socket.emit "debug", ["setting canvas: ", $scope.ann.canvas_width, $scope.ann.canvas_height]
    $scope.ann.canvas.width = $scope.ann.canvas_width
    $scope.ann.canvas.height = $scope.ann.canvas_height

    $scope.ann.ctx = $scope.ann.canvas.getContext('2d')  
    $scope.ann.ctx.lineWidth = 8;
    $scope.ann.ctx.lineCap = "round";
    $scope.ann.ctx.lineJoin = "round";
  ), 1000

  $scope.ann.colors = [
    'rgb(0,171,173)',
    'rgb(173,151,0)',
    'rgb(199,199,199)',
    'rgb(173,0,0)',
    'rgb(0,173,0)',
    'rgb(173,0,86)',
    'rgb(173,86,0)',
    'rgb(0,86,173)'
  ]
  
  setInterval (->
    if !$scope.ann.on
      return
    $scope.ann.draw()
  ), 80

  $scope.ann.touchstart = (e) ->

    $scope.ann.press()
    touchobj = e.originalEvent.changedTouches[0]
    $scope.ann.event touchobj.clientX, touchobj.clientY
    socket.emit "debug", ["touchstart event!", touchobj.clientX, touchobj.clientY]

  $scope.ann.touchmove = (e) ->

    if not ($.contains($('.history').get()[0], e.target) or $.contains($('.history').get()[1], e.target))
      e.preventDefault()
      console.log("allowing scroll")
    console.log(e)
    touchobj = e.originalEvent.changedTouches[0]
    $scope.ann.event touchobj.clientX, touchobj.clientY
    socket.emit "debug", ["touchmove event!", touchobj.clientX, touchobj.clientY]

  $scope.ann.touchend = (e) ->
    socket.emit "debug", ["touchend event!"]
    $scope.ann.unpress()

  $scope.ann.mousedown = (e) ->

    $(window).on "mousemove", $scope.ann.mousemove

    $scope.ann.press()
    $scope.ann.event e.clientX, e.clientY
    socket.emit "debug", "mousedown event!"

  $scope.ann.mousemove = (e) ->

    $scope.ann.event e.clientX, e.clientY
    socket.emit "debug", "mousemove event!"

  $scope.ann.mouseup = (e) ->

    $(window).off "mousemove", $scope.ann.mousemove
    socket.emit "debug", "mouseup event!"
    $scope.ann.unpress()

  $scope.ann.bind_events = () ->

    if $scope.tablet
      $(window).on "touchstart", $scope.ann.touchstart
      $(window).on "touchmove", $scope.ann.touchmove
      $(window).on "touchend", $scope.ann.touchend

    else
      $(window).on "mousedown", $scope.ann.mousedown
      $(window).on "mouseup", $scope.ann.mouseup

    $(document).off 'touchmove', false

  $scope.ann.unbind_events = () ->

    if $scope.tablet
      $(window).off "touchstart", $scope.ann.touchstart
      $(window).off "touchmove", $scope.ann.touchmove
      $(window).off "touchend", $scope.ann.touchend

    else
      $(window).off "mousedown", $scope.ann.mousedown
      $(window).off "mousemove", $scope.ann.mousemove
      $(window).off "mouseup", $scope.ann.mouseup

    $(document).on 'touchmove', false

  $scope.ann.press = () ->
    $scope.ann.pressed = true
    $scope.ann.press_id = $scope.generateUUID()

  $scope.ann.unpress = () ->
    $scope.ann.pressed = false
    $scope.ann.press_id = undefined

    if $scope.tablet
      $('#annotation-circle').hide()

  $scope.$watch 'ann.on', (current, old) ->
    if current
      console.log('Annotation is on')
      $scope.ann.bind_events()
    else
      console.log('Annotation is off')
      $scope.ann.unbind_events()

  # Listen for touch events from other clients
  socket.on "touch", (data) =>
    $scope.ann.update data.socketID, data.view, data.press_id, data.x, data.y

  $scope.ann.event = (x, y) ->

    if !$scope.ann.on
      return

    view = $scope.carouselIndex
    press_id = $scope.ann.press_id
    xRel = $scope.ann.toRelativeX(view, x)
    yRel = $scope.ann.toRelativeY(view, y)
    #console.log(x, y, xRel, yRel)
    
    socket.emit "touch",
      view: view
      press_id: press_id
      x: xRel
      y: yRel

    $scope.ann.update "myself", $scope.carouselIndex, press_id, xRel, yRel

  $scope.ann.update = (socketID, view, press_id, x, y) ->

    #console.log "annotation updated, x=" + x, ", y=" + y

    if !$scope.ann.data[socketID]
      $scope.ann.data[socketID] = {}

    if !$scope.ann.data[socketID][view]
    #  $scope.ann.data[socketID][view] = {}
      $scope.ann.data[socketID][0] = {}
      $scope.ann.data[socketID][1] = {}
      $scope.ann.data[socketID][2] = {}

    if !$scope.ann.data[socketID][view][press_id]
      $scope.ann.data[socketID][view][press_id] = []

    # Get number of milliseconds
    d = new Date();
    time = d.getTime();

    # Insert into annotation data object
    item = [time, x, y]
    
    # Prepend to the history (newer items first)
    $scope.ann.data[socketID][view][press_id].unshift(item)

  $scope.ann.toRelativeX = (view, x) ->
    if view == 0
      return x
    if view == 1
      return x - $('#chart').offset().left
    if view == 2
      return x - $('.slides .present').offset().left

  $scope.ann.toRelativeY = (view, y) ->
    if view == 0
      return y
    if view == 1
      return y - $('#chart').offset().top
    if view == 2
      return y - $('.slides .present').offset().top

  $scope.ann.toAbsoluteX = (view, x) ->
    if view == 0
      return x
    if view == 1
      return x + $('#chart').offset().left
    if view == 2
      return x + $('.slides .present').offset().left

  $scope.ann.toAbsoluteY = (view, y) ->
    if view == 0
      return y
    if view == 1
      return y + $('#chart').offset().top
    if view == 2
      return y + $('.slides .present').offset().top

  $scope.ann.draw = () ->

    if !$scope.ann.ctx
      return

    data = $scope.ann.data
    ctx = $scope.ann.ctx
    canvas = $scope.ann.canvas

    # Current view
    view = $scope.carouselIndex

    # Current time
    d = new Date()
    t = d.getTime()

    # Remove expired values
    for socketID in Object.keys(data)

      for press_id in Object.keys(data[socketID][view])

        press_data = data[socketID][view][press_id]

        # Find how long has passed since the last event
        latest_time = press_data[0][0]
        oldest_time = press_data[press_data.length-1][0]

        # If it hasn't been long enough to start erasing, return
        if t - latest_time < $scope.ann.waitDuration
          continue

        # If not marked yet, mark when we start erasing
        if press_data.duration == undefined
          press_data.duration = latest_time-oldest_time

        expiredInx = -1
        for aD, i in press_data

          # Check if expired
          dt = t - press_data.duration - aD[0]
          if dt > $scope.ann.trailLife

            # Expired, remove this and all following entries
            press_data.splice(i)

            # If all done, remove the whole entry
            if press_data.length == 0
              delete data[socketID][view][press_id]

            break

    if $scope.tablet

      socketID = 'myself'
      if data[socketID]

        for press_id in Object.keys(data[socketID][view])
          if $scope.ann.pressed and data[socketID][view][press_id][0]

            x = $scope.ann.toAbsoluteX(view, data[socketID][view][press_id][0][1])
            y = $scope.ann.toAbsoluteY(view, data[socketID][view][press_id][0][2])
            
            $('#annotation-circle').css('left', x - 15)
            $('#annotation-circle').css('top', y - 15)
            $('#annotation-circle').show()
    else

      ctx.clearRect(0, 0, canvas.width, canvas.height)

      for socketID, j in Object.keys(data)
        
        for press_id in Object.keys(data[socketID][view])

          points = _.map data[socketID][view][press_id], (point)->
            x = $scope.ann.toAbsoluteX(view, point[1])
            y = $scope.ann.toAbsoluteY(view, point[2])
            [x, y]
            
          points = _.flatten points
          ctx.beginPath()
          ctx.curve(points)

          ctx.strokeStyle = $scope.ann.colors[j];
          ctx.stroke()

  $scope.generateUUID = ->
    d = new Date().getTime()
    uuid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, (c) ->
      r = (d + Math.random() * 16) % 16 | 0
      d = Math.floor(d / 16)
      ((if c is "x" then r else (r & 0x7 | 0x8))).toString 16
    )
    uuid

  # ------- End Annotation ---------------------------

  $scope.sendQueryChanged = (slideNr) -> 
    $scope.sentQuery = true  
    if (slideNr?)    
      socket.emit "query",
        queryParameters: $scope.queryParameters,
        slideNumber: slideNr
    else
      socket.emit "query",
        queryParameters: $scope.queryParameters 
  #$scope.$watch('queryParameters', $scope.sendQueryChanged, true)

  $scope.showMenu = (ddvalue, ddseries, x, y, barvalue) ->
    menu = $(".drilldown__menu")
    menu.css( { "display": "inline-block", "left": x + "px", "top": y + "px" } )
    if menu.is(':empty')
      content = $compile('<p class="drilldown__title"><strong>'+ddvalue+': '+barvalue+'</strong></p>
                          <p class="drilldown__title">Drill down by</p>
                          <div class="btn-group-vertical drilldown__items">
                            <button type="button" class="btn drilldown__item" ng-repeat="ddattr in drilldownAttributes"
                              ng-click="drilldown(\''+ddvalue+'\', ddattr)">{{ddattr}}</button>
                          </div>
                ')($scope)
      menu.html(content)
      $scope.$apply()
    else
      # Swap into function
      menu.html("")
      menu.css( {"display": "none", "left": "", "top": "" } )

  $scope.drilldown = (barName, groupByCriterion) ->
    # Swap into function
    menu = $(".drilldown__menu")
    menu.html("")
    menu.css( {"display": "none", "left": "", "top": "" } )

    category = NLProcessingService.getCategoryForFilter(barName)
    newParameters = _.cloneDeep $scope.queryParameters
    for i in [0..newParameters.length-1]
      if not newParameters[i].filters[category]
        newParameters[i].filters[category] = []
      newParameters[i].filters[category].push barName.toLowerCase()
      newParameters[i].groupByCriteria[0] = groupByCriterion
    $scope.queryParameters = newParameters
    $scope.sendQueryChanged()
    $scope.state = "query"

    speechService.say "I have drilled down into #{barName} by #{groupByCriterion}"

    updateInputField()

  $scope.queryComplete = (parameters) ->
    return parameters?.indicators? and parameters?.indicators[0]?

  $scope.someQueryComplete = (parametersList, index) ->
    if !parametersList[index]
      return 0
    while !$scope.queryComplete parametersList[index]
      index++
      if !parametersList[index]
        return 0
    return index

  $scope.queryEmpty = (parameters) ->
    return (!parameters.indicators? or !parameters.indicators[0]?) and (!Object.keys(parameters.filters).length > 0)

  $scope.updateParameters = () -> 
    parameters = NLProcessingService.process($scope.recognition.currentQuery)

    #complete first query -> treat as new query
    if $scope.queryComplete parameters[0]
      #console.log "case 1"
      #copy indicator to other queries if they are incomplete
      parameters.forEach (param) -> 
        if !param.indicators[0]
          param.indicators = parameters[0].indicators

    #first query is empty -> comparison with previous
    else if parameters.length > 1 && $scope.queryEmpty parameters[0]
      #console.log "case 2"
      parameters[0] = $scope.queryParameters[0]
      if parameters[1]?.groupByCriteria[0]?
        parameters[0].groupByCriteria = parameters[1].groupByCriteria
      parameters.forEach (param) -> 
        if !param.indicators[0]
          param.indicators = parameters[0].indicators
        if !param.groupByCriteria[0]
          param.groupByCriteria = parameters[0].groupByCriteria

    #first query is partial but a later query is complete -> copy from that one (new query)
    else if $scope.someQueryComplete(parameters, 1)
      #console.log "case 3"
      completeIndex = $scope.someQueryComplete(parameters, 1)
      parameters.forEach (param) ->
        if !param.indicators[0]
          param.indicators = parameters[completeIndex].indicators

    #incomplete query only -> add parameters to previous query / queries
    else if parameters[0]?
      #console.log "case 4"
      newParameters = _.cloneDeep $scope.queryParameters
      if parameters[0].filters?
        for key, value of parameters[0].filters
          newParameters.forEach (previousParameters) ->
            previousParameters.filters[key] = value
      if parameters[0].groupByCriteria?[0]?
        newParameters.forEach (previousParameters) ->
          previousParameters.groupByCriteria[0] = parameters[0].groupByCriteria[0]
          delete previousParameters.filters[parameters[0].groupByCriteria[0]]
      parameters = newParameters
    $scope.queryParameters = parameters
    $scope.sendQueryChanged()

  $scope.getSeriesIndex = (parentIndex, childIndex) ->
    seriesIndex = 0
    i = 0
    while i < parentIndex
      seriesIndex += $scope.queryParameters[i].indicators.length
      i++
    seriesIndex += childIndex
    seriesIndex

  $scope.$watch 'recognition.emptySpeech', () -> 
    #console.log "emptySpeech:" + $scope.recognition.emptySpeech
    if $scope.recognition.emptySpeech
      $('#errorModal').modal('show')
      $scope.recognition.emptySpeech = false
  
  $scope.$watch 'recognition.currentQuery', () -> 
    $scope.entryFieldText = $scope.recognition.currentQuery

    if $scope.state is "query"
      $scope.updateParameters()
    else if $scope.state is "askForClarification"
      parameters = NLProcessingService.process($scope.recognition.currentQuery)
      wantedCategory = parameters?[0]?.groupByCriteria?[0]
      if not wantedCategory?
        return $scope.updateParameters()
      wantedFilter = $scope.queryParameters[0].filters[wantedCategory]
      if not wantedFilter?
        return $scope.updateParameters()
      for own category, filter of $scope.queryParameters[0].filters 
        if _.isEqual(filter, wantedFilter)
          if category isnt wantedCategory
            delete $scope.queryParameters[0].filters[category]
      if wantedFilter? and wantedCategory?
        speechService.say "I'm interpreting #{wantedFilter} as #{wantedCategory}"
        $scope.state = "query"
      else 
        speechService.say "Sorry. I did not understand."

  $scope.visualizeData = () ->
    # Swap into function
    menu = $(".drilldown__menu")
    menu.html("")
    menu.css( {"display": "none", "left": "", "top": "" } )

    chartType = groupByCriteriaMapping[$scope.queryParameters[0]?.groupByCriteria[0]]
    graphService.addChart($scope.resJson, $scope, chartType)


  $scope.askForClarification = (parameters)->
  
    #flip filters object
    filters = {} 
    for category, filter of parameters.filters
      filters[filter] = filters[filter] or []
      filters[filter].push category
    
    for filter, categories of filters
      if categories.length > 1
        $scope.state = "askForClarification"
        question = "Did you mean " + filter
        for category, i in categories
          if i isnt 0
            question += " or"
          question += " as " + category
          
        question += "?"
        speechService.say question 
        return true
    
    return false
      

  $scope.queryParametersChanged = (parametersList, oldParametersList)->
    return if not parametersList?[0]?

    if $scope.isNewQuery
      for parameters in $scope.queryParameters
        $scope.askForClarification(parameters)
      $scope.sendQueryChanged()
    $scope.isNewQuery = true

    $scope.resJson = $scope.retrieveData(parametersList)

    $scope.visualizeData()
    $scope.updateHistory(parametersList, oldParametersList, $scope.resJson)
    $scope.drilldownAttributes = NLProcessingService.getDrilldownAttributes(parametersList)

  $scope.$watch 'queryParameters', $scope.queryParametersChanged, true

  $scope.retrieveData = (parametersList) ->
    resultDataList = []

    for parameters in parametersList
      data = _(@lineitems) 
      for key, filter of parameters.filters
        data = data.filter (lineitem) -> ("" + lineitem[key.toUpperCase()]).toLowerCase() in filter
        
      if parameters.groupByCriteria.length > 0
        data = data.groupBy parameters.groupByCriteria[0].toUpperCase()
      else 
        data = data.groupBy "nonexistingProperty" 

      tempData = {}
      resultData = []
      for indicator in parameters.indicators
        tempData[indicator] = data.mapValues (lineitems) ->
          groupData = _(lineitems).map (lineitem) -> 
            lineitem[indicator.toUpperCase()] / 1.0
          sum = groupData
            .reduce (sum, indicatorValue) ->
              return sum + indicatorValue
          Math.round(sum * 100) / 100
          #this is a little bit hacky, should be a formatting issue really, then on the other hand it's obviously a float fuckup

        resultData.push({
          key: indicator, 
          values: convertToXYForm tempData[indicator].value()
        })

      resultDataList.push resultData
    resultDataList

  convertToXYForm = (groupedData) ->
    xyData = []
    for key, value of groupedData
      xyData.push ({
        "x": key,
        "y": value
      })

    xyData


  lineitems.then (res)->
    $scope.lineitems = res.data
    NLProcessingService.init(res.data)
    $scope.isNewQuery = false
    $scope.recognition.currentQuery = "Show me the revenue in 04/2013 by sector!"

    #$scope.recognition.currentQuery = "revenue orders germany sector compared to britain compared to france"

  updateInputField = () ->
    $scope.isNewQuery = false
    $scope.recognition.currentQuery = getAsText $scope.queryParameters

  getAsText = (parametersList) ->
    text = ""
    return text if !parametersList.length
    parametersList.forEach (item, index) ->
      if index > 0
        text += "compared to "
      text += capitalize item.indicators[0] + " "
      for key, value of item.filters
        for filter in value
          textSegments = text.split("compared to")
          if textSegments[textSegments.length-1].indexOf(capitalize filter) is -1
            text += "in " + capitalize filter + " "

    if parametersList[0].groupByCriteria?[0]?
      text += "by " + capitalize parametersList[0].groupByCriteria[0]
    return text

  capitalize = (string) ->
    string.charAt(0).toUpperCase() + string.slice 1
    
  $scope.undo = ()->
    $scope.loadFromHistory($scope.history()[$scope.currentHistoryIndex()-1])
  
  $scope.redo = ()->
    $scope.loadFromHistory($scope.history()[$scope.currentHistoryIndex()+1])
    

  $scope.loadFromHistory = (historyItem) ->
    if historyItem?.parameters?
      $scope.queryParameters = _.cloneDeep historyItem.parameters
      $scope.sendQueryChanged(historyItem.slideNr)
      $scope.goToSlide(historyItem.slideNr)
      $scope.sentSlide = true
      $scope.state = "query"
      updateInputField()

  $scope.isActiveHistoryItem = (historyItem) ->
    parametersEqual historyItem.parameters, $scope.queryParameters

  $scope.isActiveAgendaItem = (agendaItem) ->
    $scope.agendaItem == agendaItem


  $scope.agenda =
    [
      time: "12:00 PM"
      title: "Opening"
      presenter: "Stefan Kowalski"
      history:
        [ # Overall revenue in april
          parameters: [{
            indicators: ["revenue"]
            filters: []
            groupByCriteria: ["sector"]
          }]
          title: "Opening"
          slideNr: 0
        ]
    ,
      time: "12:20 PM"
      title: "Finance & Administration"
      presenter: "Claudia Exeler"
      history:
        [ # Overall revenue in april
          parameters: [{
            indicators: ["revenue"]
            filters:
              period: ["04/2013"]
            groupByCriteria: ["sector"]
          }]
          title: "Revenue Analysis"
          slideNr: 1
        , # Overall revenue april, march, february
          parameters: [{
            indicators: ["revenue"]
            filters:
              period: ["04/2013"]
            groupByCriteria: ["sector"]
          }, {
            indicators: ["revenue"]
            filters:
              period: ["03/2013"]
            groupByCriteria: ["sector"]
          }, {
            indicators: ["revenue"]
            filters:
              period: ["02/2013"]
            groupByCriteria: ["sector"]
          }]
          slideNr: 2
          title: "Sector overview"
        ,  # Overall revenue april, march, february
          parameters: [{
            indicators: ["revenue"]
            filters:
              sector: ["infrastructure & cities"]
            groupByCriteria: ["period"]
          }]
          slideNr: 3
          title: "Monthly Comparison"
        ,# Revenue in I&C by division
          parameters: [{
            indicators: ["revenue"]
            filters:
              period: ["04/2013"]
              sector: ["infrastructure & cities"]
            groupByCriteria: ["division"]
          }]
          title: "Compare by division"
          slideNr: 4
        ]
    ,
      time: "1:00 PM"
      title: "Update in China"
      presenter: "Hayk Martirosyan"
      history: [
        parameters: [{
          indicators: ["revenue"]
          filters:
            country: ["china"],
            lead_country: ["china"]
            sector: ["infrastructure & cities"]
          groupByCriteria: ["division"]
        }]
        slideNr: 5
        title: "Update in China"
      ]
      ,
        time: "12:55 PM"
        title: "Future Operations"
        presenter: "Jossekin Beilharz"
        history:
          [ # Overall revenue in april
            parameters: [{
              indicators: ["orders"]
              filters:
                period: ["03/2013"]
              groupByCriteria: ["sector"]
            }, {
              indicators: ["revenue"]
              filters:
                period: ["04/2013"]
              groupByCriteria: ["sector"]
            }]
            title: "Future operations"
            slideNr: 6
          ]
    ]
  $scope.agendaItem = $scope.agenda[0]
  #$scope.loadFromHistory($scope.history(0))

  $scope.updateHistory = (newParametersList, oldParametersList, visualizationData) ->
    # we don't want duplicate history items. if the question has been asked before, we will delete it
    #return if _.find $scope.history(), (historyItem) ->
    #  _.isEqual historyItem.parameters, newParametersList, parametersEqual
    # save question to history at the current history index

    for agendaItem in $scope.agenda
      continue if not agendaItem.history
      return if _.find agendaItem.history, (historyItem) ->
        _.isEqual historyItem.parameters, newParametersList, parametersEqual
    #  for historyItem in agendaItem.history
    #    if parametersEqual newParametersList, historyItem.parameters
    #      return

    $scope.history().splice($scope.historyIndex(oldParametersList) + 1, 0, {
      parameters: _.cloneDeep newParametersList
      visualizationData: visualizationData
    })


  parametersEqual = (a, b) ->
    return false if b == undefined
    return false if a.length != b.length 
    return false if not _.isEqual a[0].groupByCriteria[0], b[0].groupByCriteria[0]
    for index in [0..a.length-1]
      return false if not _.isEqual a[index].indicators, b[index].indicators
      for key, value of a[index].filters
          return false if not _.isEqual b[index].filters[key], value
      for key, value of b[index].filters
          return false if not _.isEqual a[index].filters[key], value
    return true
    
  $scope.currentHistoryIndex = ()->
    return $scope.historyIndex($scope.queryParameters)

  $scope.historyIndex = (parameters) ->
    _.findIndex $scope.history(), (historyItem) ->
      _.isEqual historyItem.parameters,  parameters, parametersEqual


  $scope.queryDatabase = (sql) -> 
    #ajax-get data.
    return @testAnswerData


  #NOW SOME SPEECH RECOGNITION
  $scope.recognition = speechService.init()
  console.log("current query initially: " + $scope.recognition.currentQuery)


  $scope.askButtonDown = ->
    $('#errorModal').modal('hide')
    $('#explanationModal').modal('show')
    $scope.recognition.startRecognition()

  $scope.askButtonUp = ->
    $('#explanationModal').modal('hide')
    $scope.recognition.stopRecognition()
    

  $('#askButton').on 'touchstart', $scope.askButtonDown
  $('#askButton').on 'touchend', $scope.askButtonUp


  $scope.removeFromQueryParameters = (parameter, parameterValue, series) ->
    console.log("removeFromQueryParameters")
    newParameters = _.cloneDeep $scope.queryParameters

    if parameter == "indicators"
      console.log "remove indicators"
      unless newParameters.length <= 1
        newParameters.splice(series,1)
        #speechService.say("I have removed " + parameterValue) #come up with sth useful to say 
    else if parameter == "groupByCriteria"
      newParameters.forEach (param) -> param.groupByCriteria = _.without(newParameters[series].groupByCriteria, parameterValue)
      speechService.say("I have removed the grouping by " + parameterValue)
    else if parameter == "filters"
      for key, filter of newParameters[series].filters
        newParameters[series].filters[key] = _.without(filter, parameterValue)
        if (newParameters[series].filters[key].length == 0)
          delete newParameters[series].filters[key]

      speechService.say("Ok, I have removed " + parameterValue)

    $scope.queryParameters = newParameters
    updateInputField()
    $scope.sendQueryChanged()

  $scope.recognize = () ->
    $scope.recognition.say("test")

  $scope.setAgendaItem = (agendaItem) ->
    $scope.agendaItem = agendaItem

  $scope.history = () ->
    $scope.agendaItem.history or []

  #Not nice, i know... 
  $(window).scroll ()-> 
    $(window).scrollTop 0
    $(window).scrollLeft 0


  updateCurrentAgendaItem = ->
    for agendaItem in $scope.agenda
      continue if not agendaItem.history?
      for historyItem in agendaItem.history
        if parametersEqual $scope.queryParameters, historyItem.parameters
          $scope.agendaItem = agendaItem
          $scope.$apply()

  return



