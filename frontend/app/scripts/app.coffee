touchvizHtml = '<svg class="touchviz" height="50" width="50"><circle r="20" transform="translate(25,25)"></circle></svg>'

angular.module('touchViz',[]).run ($swipe) ->
module = angular.module('botfApp', 
    [
      'ngRoute',
      'ngAnimate',
      'angular-carousel',
      'touchViz'
    ]).run ->
  FastClick.attach document.body
  return


module.config ($routeProvider) ->
  $routeProvider
  .when "/agenda",
    carouselIndex: 0
  .when "/data",
    carouselIndex: 1
  .when "/presentation",
    carouselIndex: 2
  .otherwise redirectTo: "/agenda"

module.run ($rootScope, $routeParams) ->
  $rootScope.alert = (text)->
    alert(text);

  # Disable scrolling
  $("body").css "overflow", "hidden"
  #$(document).bind 'touchmove', false

  isTablet = () ->
    return (
        # Detect iPhone
        (navigator.platform.indexOf("iPhone") != -1) ||
        # Detect iPod
        (navigator.platform.indexOf("iPod") != -1) || 
        # Detect iPad
        (navigator.platform.indexOf("iPad") != -1)
    )

  $rootScope.tablet = isTablet()
  
  $rootScope.routeParams = $routeParams
  $rootScope.$watch 'routeParams', -> 
    if $routeParams.tablet is "true"
      $rootScope.tablet = true
    else if $routeParams.tablet is "false"
      $rootScope.tablet = false
    else
      $rootScope.tablet = isTablet()
  , true

module.filter 'flattenFilters', ()->
  (queryParameters)->
    #if _(queryParameters?.alternativeFilters).keys().some()
    #  _(queryParameters?.filters)
    #    .pairs()
    #    .map((p)-> p[0] + " " + p[1])
    #    .value()
    #else
      _(queryParameters?.filters)
        .values()
        .flatten()
        .unique()
        .value()

module.directive "selectOnClick", ->
  restrict: "A"
  link: (scope, element, attrs) ->
    element.on "click", ->
      @select()