keywords = {}
keywords.indicators = ['revenue', 'orders']
keywords.filters = {}
# keywords.filters = _.union(keywords.filters.countries, keywords.filters.products, keywords.filters.sectors, keywords.filters.years)

keywords.categories = [
  ['lead_country', 'country']
  ['period']
  ['sector', 'division', 'business unit', 'business segment', 'business subsegment']
]
keywords.groupByCriteria = keywords.categories.reduce( (a,b) -> 
  a.concat b
)

normalizationMapping = 
  'october': '10/2012'
  'november': '11/2012'
  'december': '12/2012'
  'january': '01/2013'
  'february': '02/2013'
  'march': '03/2013'
  'april': '04/2013'
  'may': '05/2013'
  'june': '06/2013'
  'july': '07/2013'
  'august': '08/2013'
  'september': '09/2013'

  'cash flow': 'cashflow'
  'bisector': 'by sector'
  'infrastructure': 'infrastructure & cities'
  '&': 'and'
  'lead country': 'lead_country'
  'nation': 'country'
  'usa': 'united states'
  'health': 'healthcare' 
  'result': 'revenue'
  'britain': 'united kingdom'
  'england': 'united kingdom'
  'uk': 'united kingdom'
  'uae': 'united arab emirates'
  'gross': 'revenue'
  'receipts': 'revenue'
  'state': 'country'
  'land': 'country'
  'area': 'country'
  'month': 'period'
  'year': 'period'
  'quarter': 'period'
  'time': 'period'
  'trend': 'period'
  'history': 'period'
  'course': 'period'
  'read country': 'lead_country'
  'leave country': 'lead_country'

module = angular.module('botfApp')
module.service 'NLProcessingService', (accessDatabaseService)->

  @init = (data) ->
    for category in keywords.groupByCriteria
      keywords.filters[category] = 
        _(data)
          .groupBy(category.toUpperCase())
          .keys()
          .compact()
          .value()
    #keywords.filters.country[keywords.filters.country.indexOf 'IN'] = 'INDIA'

  @getDisplayFormat = (parameters) -> 
    queryString = parameters.indicators[0]
    for key of parameters.filters
      queryString += ' in ' + parameters.filters[key]
    queryString += ' by ' + parameters.groupByCriteria[0]

  @getDrilldownAttributes = (parametersList) ->
    ddattrsList = []
    for parameters in parametersList
      ddattrs = []
      for ddcategory in keywords.categories
        if _.contains(ddcategory, parameters.groupByCriteria[0])
          index = _.indexOf(ddcategory, parameters.groupByCriteria[0])
          # next deepest class
          if index + 1 < ddcategory.length
            ddattrs.push ddcategory[index + 1]
        else
          i = 0
          while ddcategory[i]
            if _.isEmpty(parameters.filters[ddcategory[i]])
              ddattrs.push ddcategory[i]
              break
            i++
      ddattrs.forEach (ddattr) ->
        if !_.contains(ddattrsList, ddattr)
          ddattrsList.push ddattr

    ddattrsList

  @getCategoryForFilter = (filter)->
    for category, filters of keywords.filters
      if filter in filters
        return category

  @succeeds = (text, token, predecessors) ->
    _.some(predecessors, (e) -> text.substring(0, text.indexOf(token)).match(new RegExp('\\b' + e + '\\b ?$')))
 
  @process = (input, options) -> 
    if(!input?)
      return 1
    defaults = 
      method: 'wordexists'
    options = $.extend({}, defaults, options)

    input = input.toLowerCase()

    for own searchVal, newVal of normalizationMapping
      input = input.replace new RegExp('\\b' + searchVal + '\\b'), newVal

    input = input.replace '.', 'period'
    input = input.toLowerCase()
    subinputs = input.split(/compare/gi)
    #console.log subinputs

    resList = []
    for subinput in subinputs
      res = 
        indicators: []
        filters: {}
        groupByCriteria: []


      match = _.map keywords['indicators'], (word) -> ("" + word) #convert to strings
      for keyword in match
        if subinput.match(new RegExp('\\b' + keyword + '\\b'))
          res['indicators'].push keyword

      for key of keywords.filters
        match = _.map keywords.filters[key], (word) -> ("" + word).toLowerCase()
        val = []
        for keyword in match
          if subinput.match(new RegExp('\\b' + keyword + '\\b'))
            val.push keyword
        unless _.isEmpty(val)
          res.filters[key] = val
      toSingleQueries(res).forEach (q) ->
        resList.push q
    
    match = _.map keywords['groupByCriteria'], (word) -> ("" + word) #convert to strings
    for keyword in match
      if input.match(new RegExp('\\b' + keyword + '\\b'))
        resList.forEach (res) ->
          res['groupByCriteria'].push keyword

    resList

  toSingleQueries = (parameters) ->
    results = [parameters]
    length = parameters.indicators.length

    #if length > 0
    #  results.push parameters
    
    if length > 1

      oldindicators = parameters.indicators
      parameters.indicators = [oldindicators[0]]

      for i in [1..length-1]
        newQuery = _.cloneDeep parameters 
        newQuery.indicators = [oldindicators[i]]
        results.push newQuery

    results

  @distanceMetrics = {}
  @distanceMetrics.levenshtein = (str1, str2) ->
      max = Math.max
      min = Math.min
      l1 = str1.length
      l2 = str2.length
      return max(l1, l2)  if min(l1, l2) is 0
      i = 0
      j = 0
      d = []
      i = 0
      while i <= l1
        d[i] = []
        d[i][0] = i
        i++
      j = 0
      while j <= l2
        d[0][j] = j
        j++
      i = 1
      while i <= l1
        j = 1
        while j <= l2
          d[i][j] = min(d[i - 1][j] + 1, d[i][j - 1] + 1, d[i - 1][j - 1] + ((if str1.charAt(i - 1) is str2.charAt(j - 1) then 0 else 1)))
          j++
        i++
      d[l1][l2]

  @distanceMetrics.soundex = (s) ->
    a = s.toLowerCase().split("")
    f = a.shift()
    r = ""
    codes =
      a: ""
      e: ""
      i: ""
      o: ""
      u: ""
      b: 1
      f: 1
      p: 1
      v: 1
      c: 2
      g: 2
      j: 2
      k: 2
      q: 2
      s: 2
      x: 2
      z: 2
      d: 3
      t: 3
      l: 4
      m: 5
      n: 5
      r: 6


    r = f + a.map((v, i, a) ->
      codes[v]
    ).filter((v, i, a) ->
      (if (i is 0) then v isnt codes[f] else v isnt a[i - 1])
    ).join("")
    (r + "000").slice(0, 4).toUpperCase()

  @distanceMetrics.wordexists = (reference, input) ->
    reference = reference.toLowerCase().trim()
    input = input.toLowerCase().trim()
    
    words = reference.match(/\b\S+\b/gi)
    return Number.MAX_VALUE unless words

    matches = 0
    for word in words
      if input.indexOf(word) != -1
        matches++
    res = (words.length - matches) / words.length
    res
  
  this

module.service 'accessDatabaseService', ($http)->
  @runQuery = (query)->
    #http get /sql/query
