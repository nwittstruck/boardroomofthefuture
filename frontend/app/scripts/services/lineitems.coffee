module = angular.module('botfApp')

#this service returns a promise to the data being loaded
module.service 'lineitems', ($http) ->
  $http.get('data/revenue.json')