module = angular.module('botfApp')
module.service 'speechService', ($rootScope)->
  @init = () =>
    console.log("initializing speech kit")
    document.addEventListener('deviceready', this.onDeviceReady, false);
    @currentQuery = "hest"
    return this

  @onDeviceReady = () =>
    serverURL = "sandbox.nmdp.nuancemobility.net";
    @speechKit = new NuanceSpeechKitPlugin();
    @speechKit.initialize "Credentials", serverURL, 443, false, ((r) ->
      @printRecoResult r
      return
    ), (e) ->
      @printRecoResult e
      return

  @startRecognition = () =>
    if (!@speechKit)
      return

    console.log("starting recognition")

    recoType = "dictation"
    recoLanguage = "en_us"
    @speechKit.startRecognition recoType, recoLanguage, ((r) =>
      @printRecoResult r
      return
    ), (e) ->
      @printRecoResult e
      return


  @stopRecognition = () =>
    if (!@speechKit)
      @emptySpeech = true
      return 
    console.log("stopping recognition")
    @speechKit.stopRecognition ((r) =>
      @printRecoResult r
      return
    ), (e) ->
      console.log e
      $rootScope.$apply =>
          @emptySpeech = true
          @currentQuery = ""
          return

  @printRecoResult = (resultObject) ->
    console.log(resultObject)

    if resultObject.event is "RecoComplete" or resultObject.event is "RecoError"
      if resultObject.results is `undefined`
        $rootScope.$apply =>
          @emptySpeech = true
          return        
      else
        console.log("detected: "+ resultObject.results[0].value)
        console.log($rootScope)
        $rootScope.$apply =>
          @currentQuery = resultObject.results[0].value
          return

  @say = (text) =>
    console.log("saying "+text)
    if (@speechKit)
      console.log("speech kit present")
      @speechKit.playTTS text, "en_US", "Serena", ((r) ->
      ), (e) ->
    else
      console.log("speech kit not present")

    return true

  return this