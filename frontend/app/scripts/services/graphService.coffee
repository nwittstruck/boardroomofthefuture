height = 400
width = 600

module = angular.module('botfApp')
module.service 'graphService', ()->
  
  commafy = (integer) -> 
    integer.replace(/(\d)(?=(\d{3})+$)/g, '$1,');

  formatNumber = (number) ->
    number = number
    .toFixed(2)
    .split('.')
    commafy(number[0]) + ' €'

  euroFormat = d3.format(",.0f")
  
  
  @addChart = (dataObject, scope, chartType) ->
    if dataObject[0] && chartType == 'barchart'
      if dataObject[0][0].values.length > 5
        addHorizontalBarChart(dataObject, scope)
      else 
        addBarChart(dataObject, scope)
    else if chartType == 'linechart'
      addLineChart(dataObject, scope)
    else if chartType == 'countrymap'
      addCountryMap(dataObject, scope)
    else if chartType == 'number'
      addNumber(dataObject, scope)
    else 
      console.log "No visualization is available for " + chartType
      addHorizontalBarChart(dataObject, scope)
  
  colors = [
    "#33BCBD"
    "#C80909"
    "#DAC32D"
    "#33BD33"
    "#BD3378"
    "#929292"
  ]
  ###"#00ABAD"
    "#777777"
    "#AD0000"
    "#AD9700"
    "#00AD00"
    "#AD0056"###
    
  convertData = (dataObjectsList) ->
    series = []
    for dataObject in dataObjectsList
      for dataItem, i in dataObject
        dataPoint = { 
          key: dataItem.key + " " + (series.length + 1), 
          values: dataItem.values
          color: colors[series.length]
        }
        series.push dataPoint
    series

  convertTimeData = (dataObjectsList) ->
    series = []
    for dataObject in dataObjectsList
      for i in [0..dataObject.length-1] by 1
        newValues = []
        for value in dataObject[i].values
          newValues.push {
            x: +(d3.time.format("%m/%Y").parse value.x)
            y: value.y
          }
        newValues.sort (a,b) -> (a.x - b.x)

        lineData = { 
          key: dataObject[i].key + (series.length+1), 
          values: newValues
          color: colors[series.length]
        } 
        series.push lineData

    series

  addBarChart = (dataObject, rootScope) ->
    nv.addGraph () -> 

      chart = nv.models.multiBarChart()
        .showControls(false)
        .showLegend(false)
        .reduceXTicks(false)
        .staggerLabels(true)
        .tooltips(false)
        .width(875)
        .height(365)
        .margin({top: 22, right: 0, bottom: 12, left: 130})
        .forceY([0])
        #.noData('The actual Data View controls combination results in zero data, restore Revenue.')

      chart.multibar.dispatch.on('elementClick', (e) ->
        drillDownMenuXY = getDrillDownMenuPosition()
        rootScope.showMenu e.point.x, e.seriesIndex, drillDownMenuXY[0], drillDownMenuXY[1], formatNumber e.point.y
      )

      chart.yAxis
        .axisLabel("Euro")
        .axisLabelDistance(40)
        .tickFormat(euroFormat)

      $('.charts').html("")
      d3.select(".charts").attr("id", "chart").append("svg")
        .datum(convertData dataObject)
        .call(chart)
        .attr('height', height)
        .attr('width', 875)
      
      chart

  addHorizontalBarChart = (dataObject, rootScope) ->
    nv.addGraph () -> 

      chart = nv.models.multiBarHorizontalChart()
        .showControls(false)
        .showValues(true)
        .showLegend(false)
        .tooltips(false)
        .width(875)
        .height(365)
        .margin({top: 22, right: 0, bottom: 12, left: 300})
        .forceY([0])

      chart.multibar.dispatch.on('elementClick', (e) ->
        drillDownMenuXY = getDrillDownMenuPosition()
        rootScope.showMenu e.point.x, e.seriesIndex, drillDownMenuXY[0], drillDownMenuXY[1], formatNumber e.point.y
      )

      chart.yAxis
        .axisLabel("Euro")
        .tickFormat(euroFormat)

      $('.charts').html("")
      d3.select(".charts").attr("id", "chart").append("svg")
        .datum(convertData dataObject)
        .call(chart)
        .attr('height', height)
        .attr('width', 875)        
      
      chart


  addLineChart = (dataObject, rootScope) ->
    nv.addGraph () -> 

      chart = nv.models.lineChart()
        .tooltips(false)
        .showLegend(false)
        .width(875)
        .height(365)
        .margin({top: 22, right: 0, bottom: 12, left: 130})
        .forceY([0])
        .useInteractiveGuideline(true)

      chart.xAxis
        .tickFormat (d) -> d3.time.format("%m/%Y") (new Date(d))

      chart.yAxis
        .axisLabel("Euro")
        .axisLabelDistance(40)
        .tickFormat(euroFormat)

      $('.charts').html("")
      d3.select(".charts").attr("id", "chart").append("svg")
        .datum(convertTimeData dataObject)
        .call(chart)
        .attr('height', height)
        .attr('width', 875)        

      d3.selectAll('.nv-point').on('click', (e) ->
        drillDownMenuXY = getDrillDownMenuPosition()
        rootScope.showMenu ( d3.time.format("%m/%Y") (new Date(e.x)) ), e.seriesIndex, drillDownMenuXY[0], drillDownMenuXY[1], formatNumber e.y
      )

      chart

  addNumber = (dataObject, rootScope) ->
    $('.charts').html("")
    $('.charts').append("<div class='data-view__numbervis'></div>")
      .height(height)


    i = 0
    for dataItem in dataObject
      $('.data-view__numbervis').append("<div id='numbervis-" + i + "'></div>")
      if dataItem[0]?.values[0]?
        value = dataItem[0].values[0].y
      else
        value = 0
      $('#numbervis-'+i)
        .html(formatNumber value)
        .attr('class', 'data-view__numbervis--series-' + i + '-' + dataObject.length)
        .on('click', (e) ->
          offset = $('.charts').offset()
          rootScope.showMenu (capitalize dataItem[0].key), i, e.pageX - offset.left, e.pageY - offset.top, formatNumber value
        )
      i++

  capitalize = (string) ->
    string.charAt(0).toUpperCase() + string.slice 1

  addCountryMap = (dataObject, rootScope) ->

    m_width = $("#chart").width()
    width = 938
    countryMapHeight = 500
    proportionalHeight = m_width * countryMapHeight / width

    projection = d3.geo.mercator()
      .scale(150)
      .translate([width / 2, countryMapHeight / 1.5])

    path = d3.geo.path()
      .projection(projection)

    $('#chart').html("")

    svg = d3.select("#chart")
      .append("svg")
      .attr("preserveAspectRatio", "xMidYMid")
      .attr("viewBox", "0 0 " + width + " " + countryMapHeight)
      .attr("width", m_width)
      .attr("height", m_width * countryMapHeight / width)
      .attr("class", "chart__svg")

    g = svg.append("g");

    d3.json "data/countries-botf.topo.json", (error, json) ->

      topojsonData = topojson.feature(json, json.objects.countries).features

      # Fixed revenue, Orders are missing
      revenueData = dataObject[0][0].values

      console.log 'in AddCountryMap dataObject: ' + revenueData

      # This countries exists in the dataObject and should be visualized
      relevantCountries = _.pluck revenueData, 'x'
      console.log 'All countries which SHOULD be visualized ' + relevantCountries.length + ' ' + _.sortBy relevantCountries

      # Prepare revenue range
      revenueValues = _.sortBy _.pluck revenueData, 'y'
      revenueMin = _.min revenueValues
      console.log "revenue min " + revenueMin
      revenueMax = _.max revenueValues
      console.log "revenue max " + revenueMax

      # Build subset of topojson data for countries which are relevant in this view
      topojsonRelevantCountries = _.filter topojsonData, (country) -> _.contains relevantCountries, country.properties.COUNTRY
      topojsonRelevantCountriesCount = topojsonRelevantCountries.length

      resultCountries = []
      console.log
      _.forEach topojsonRelevantCountries, (country) -> resultCountries.push country.properties.COUNTRY

      console.log 'Relevant topo json country objects ' + topojsonRelevantCountries.length + ' ' + _.sortBy resultCountries

      inVisualizationMissingCountries = _.difference( relevantCountries, resultCountries )
      console.log 'Visulization misses: ' + inVisualizationMissingCountries

      innerG = g.append("g")
        .attr("id", "countries")

      sel = innerG.selectAll("path")
        .data(topojsonRelevantCountries)
        .enter()
        .append("g")
        .attr("id", (d) -> (d.properties.COUNTRY_CODE))

      # Country Code
      sel.append("text")
        .attr("transform", (d) -> ("translate(" + path.centroid(d) + ")"))
        .attr("text-anchor", "middle")
        .attr("class", "country__code")
        .text( (d) -> (d.properties.COUNTRY_CODE) )

      # Map revenue from country in topojson to revenue from country in revenueData
      actualCountryRevenue = (d) -> _.find(revenueData, (country) -> country.x == d.properties.COUNTRY).y

      # Revenue
      sel.append("text")
        .attr("transform", (d) -> ("translate(" + path.centroid(d) + ")"))
        .attr("class", "country__revenue")
        .attr("dy", "1.5em")
        .attr("text-anchor", "middle")
        #.text( (d) -> "€ " + euroFormat( actualCountryRevenue(d) ) )

      colorscale = d3.scale.linear()
        .domain([revenueMin,revenueMax])
        .range(['#fff','#009999'])

      colorscale2 = d3.scale.category20b()
        .domain([revenueMin,revenueMax])

      # Circles
      # From http://blog.safaribooksonline.com/2014/02/11/d3-js-maps/

      radiusMap = d3.scale.linear()
        .domain([revenueMin, revenueMax])
        .range([1, 70])

      # Create circles
      sel.append("circle")
        .each( (d) ->
          d.properties.r = radiusMap( actualCountryRevenue(d) )
          d.properties.c = path.centroid(d)
          d.properties.x = m_width
          d.properties.y = m_width * countryMapHeight / width )
        .attr("cx", (d) -> d.properties.x + d.properties.c[0] - m_width)
        .attr("cy", (d) -> d.properties.y + d.properties.c[1] - proportionalHeight)
        .attr("r", (d) -> d.properties.r)
        .attr("fill", (d) -> colorscale2( actualCountryRevenue(d) ))
        .attr("class", "country__circle")
      # End Circles

      # Circles collision detection

      # End circles collision detection

      # Country Path
      sel.append("path")
        #.attr("id", (d) -> (d.properties.COUNTRY_CODE))
        .attr("class", "country")
        .attr("d", path)
        .attr("fill", (d) -> colorscale2( actualCountryRevenue(d) ))
        .on('click', (e) ->
          drillDownMenuXY = getDrillDownMenuPosition()
          rootScope.showMenu e.properties.COUNTRY, e.seriesIndex, drillDownMenuXY[0], drillDownMenuXY[1], formatNumber e.properties.y
        )
      # End Country


  getDrillDownMenuPosition = (isNumber) ->
    coordinates = [0, 0]
    
    mainChart = d3.select( ".main-chart" ).node()
    coordinates = d3.mouse( mainChart )

    # Prevent hanging the drill down menu off the visible right screen area
    if coordinates[0] > 700
      coordinates[0] = 700

    coordinates

  this